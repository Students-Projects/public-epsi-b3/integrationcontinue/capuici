package Eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TriTableauTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	//@Test
	//void testTrier() {
	//	fail("Not yet implemented");
	//}

	@Test
	void testTriCroissant(){

		int[] a = {0,1,7,3,4,6,8,2,9,5};

		TriTableau.triCroissant(a);

		for (int i=0; i<9; i++)
		{
			assertEquals(a[i], i);
		}

	}

	@Test
	void testTriDecroissant() {
		int[] a = {0,1,7,3,4,6,8,2,9,5};

		TriTableau.triDecroissant(a);

		for (int i=a.length; i>0; i--)
		{
			assertEquals(a[i], i);
		}
	}

}
