package Eval;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

class FIFOTest {


	@BeforeEach
	void setUp() throws Exception {

	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAdd()
	{
		FIFO blbl = new FIFO();
		blbl.add(12);
		assertEquals(12, blbl.first());
	}

	@Test
	void testFirst() {

		FIFO blbl = new FIFO();
		blbl.add(12);
		blbl.add(19);
		blbl.add(53);
		blbl.add(95);
		blbl.add(1);
		blbl.add(97);
		assertEquals(12, blbl.first());
	}

	@Test
	void testIsEmpty() {
		FIFO blbl = new FIFO();
		assertTrue(blbl.isEmpty());
	}

	@Test
	void testIsNotEmpty() {

		FIFO blbl = new FIFO();
		blbl.add(12);
		blbl.add(19);
		blbl.add(53);
		blbl.add(95);
		blbl.add(1);
		blbl.add(97);

		assertFalse(blbl.isEmpty());
	}

	@Test
	void testRemoveFirst() {

		FIFO blbl = new FIFO();
		blbl.add(12);
		blbl.add(19);
		blbl.add(53);
		blbl.add(95);
		blbl.add(1);
		blbl.add(97);

		int a = 97;
		blbl.removeFirst();
		assertNotEquals(a ,blbl.first());
	}

	@Test
	void testSize() {
		FIFO blbl = new FIFO();
		blbl.add(12);
		blbl.add(19);
		blbl.add(53);
		blbl.add(95);
		blbl.add(1);
		blbl.add(97);
		assertEquals(6, blbl.size());
	}

}
