# Capuici

## Installallation Jenkins

Créer un nouveau dossier et y créer le fichier `Dockerfile`

```bash
    mkdir Jenkins
    cd Jenkins
    touch Dockerfile
```

Le fichier Dockerfile est le contructeur du conteneur que nous allons lancer, celui-ci se présentera ce cette façon:

```Dockerfile
    FROM jenkins/jenkins-experimental:blueocean-jdk11
    
    USER root
    RUN apt update && apt upgrade -y
    RUN apt install -y ruby make git -y
    RUN apt install openjdk-11-jdk openjdk-11-jre -y
    
    USER jenkins
```

Après ça on execute la commande suivante afin de construire notre conteneur de base:

```bash
    $ docker build -t jenkins .
```

Nous allons maintenant créer un fichier `launch` qui contiendra la comande executable:

```bash
    #!/bin/bash
    
    docker run \
      -u root \
      --rm \
      --name jenkins\
      -d \
      -p 65432:8080 \
      -p 50000:50000 \
      -v jenkins-data:/var/jenkins_home \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -it jenkins
```

Lancer ensuite le conteneur après avoir rendu le fichier de lancement executable:

```bash
    $ chmod +x launch
    $ ./launch
```

<i>Si besoin ``docker exec -it jenkins bash`` vous permettra d'entrer dans les commande du docker.</i>

>
>Votre Interface jenkins est maintenant disponible à l'adresse: [http://127.0.0.1:65432](http://127.0.0.1:65432)
>
____

Pour tout renseignement sumplémentaire, merci de se réferer au [wiki](https://gitlab.com/Students-Projects/public-epsi-b3/integrationcontinue/capuici/wikis/home).

